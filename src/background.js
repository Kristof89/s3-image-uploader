'use strict'

import { app, protocol, BrowserWindow, ipcMain } from 'electron'
import * as path from 'path'
import { format as formatUrl } from 'url'
import Jimp from 'jimp'
import {
  createProtocol,
  installVueDevtools
} from 'vue-cli-plugin-electron-builder/lib'
const isDevelopment = process.env.NODE_ENV !== 'production'
if (isDevelopment) {
  // Don't load any native (external) modules until the following line is run:
  require('module').globalPaths.push(process.env.NODE_MODULES_PATH)
}

// global reference to mainWindow (necessary to prevent window from being garbage collected)
let mainWindow

// Standard scheme must be registered before the app is ready
protocol.registerStandardSchemes(['app'], { secure: true })
function createMainWindow () {
  const window = new BrowserWindow({ webPreferences: { webSecurity: false }})

  if (isDevelopment) {
    // Load the url of the dev server if in development mode
    window.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) window.webContents.openDevTools()
  } else {
    createProtocol('app')
    //   Load the index.html when not in development
    window.loadURL(
      formatUrl({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
      })
    )
  }

  window.on('closed', () => {
    mainWindow = null
  })

  window.webContents.on('devtools-opened', () => {
    window.focus()
    setImmediate(() => {
      window.focus()
    })
  })

  return window
}

// quit application when all windows are closed
app.on('window-all-closed', () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // on macOS it is common to re-create a window even after all windows have been closed
  if (mainWindow === null) {
    mainWindow = createMainWindow()
  }
})

// create main BrowserWindow when electron is ready
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    await installVueDevtools()
  }
  mainWindow = createMainWindow()
})

ipcMain.on('send-data-to-main-trigger', (event, arg) => {
console.log(arg)
let userData = app.getPath('userData')
console.log(userData)
let imageName = path.basename(arg[0]);
console.log(imageName)
let sanitizeImageUrl = arg[0].replace(imageName, '').replace(/\\/g,'_').replace(/:/g,'')
console.log(sanitizeImageUrl)
let imageSavePath = path.join(userData + '/thumbnails/' + sanitizeImageUrl)
console.log(imageSavePath)


  for(var a = 0; a < arg.length; a++) {

    let imageName = path.join('/' + path.basename(arg[a]));

    console.log("loading image");
    Jimp.read(arg[a]).then(image => {
      image
        .resize(300, 300)
        .writeAsync(imageSavePath + imageName)
    }).then(() => {
      console.log("callback");
      event.sender.send("send-data-to-main-triggered", true)
    }).catch(error => {
      console.log(error);
    })
  }
})